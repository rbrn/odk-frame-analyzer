import base64

import chardet
from PIL.Image import Image
from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

from garbage_detection import GarbageImageClassifier
# Import garbage classifier

app = Flask(__name__)
#app.config['UPLOAD_FOLDER']
#app.config['MAX_CONTENT_PATH']
classifier = GarbageImageClassifier()
@app.route('/upload')
def upload():
    return render_template('upload.html')

@app.route('/uploader', methods = ['GET', 'POST'])
def uploader():
    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename(f.filename))

        with open("./images/bucuresti_garbage.jpeg", "rb") as image_file:
            encoded_string = str(base64.b64encode(image_file.read())).replace("b'",'').replace("'","")
        print(encoded_string)
        frame = classifier.detect_image(encoded_string)
        return frame

if __name__ == '__main__':
    app.run(debug = True)