from datetime import date, datetime
from frame import RawFrame
import os
import base64
import socket
from pathlib import Path

def save_file(analysed_frame: RawFrame, something_detected=False, privacy_filter_off=True):

    machine_hostname = socket.gethostname()

    folder_path = '/odk/frames/{0}'.format(machine_hostname)

    stream_id = analysed_frame.stream_id
    img = analysed_frame.img
    timestamp = datetime.strftime(analysed_frame.taken_at, "%Y-%m-%dT%H:%M:%S.%f")
    timestamp_date = datetime.strftime(analysed_frame.taken_at, "%Y-%m-%d")
    gps_lat = analysed_frame.lat_lng['lat']
    gps_lng = analysed_frame.lat_lng['lng']

    folder_path: str

    if something_detected:
        folder_path = '{0}/something'.format(folder_path)
    else:
        folder_path = '{0}/nothing'.format(folder_path)
    
    # Set file name and location
    gps_location = "{0}_{1}".format(gps_lat, gps_lng)
    filepath = "{0}/{1}/{2}".format(folder_path,
                                    timestamp_date,
                                    stream_id)

    filepath_processed = "{0}/{1}/{2}/processed_images".format(folder_path,
                                    timestamp_date,
                                    stream_id)

    if not os.path.exists(filepath):
        os.makedirs(filepath)

    if not os.path.exists(filepath_processed):
        os.makedirs(filepath_processed)

    if img.find(',') > -1:
        b64_clean = img.split(',')[1]
    else:
        b64_clean = img

    # Save original image if privacy check is fullfilled
    filename = "{0}_{1}.jpg".format(timestamp, gps_location)
    full_filename = "{0}/{1}".format(filepath, filename)
    if something_detected == False:
        original_image = base64.b64decode(b64_clean)
        save_b64_image(full_filename, original_image)
    elif privacy_is_okay(analysed_frame.object_count) or privacy_filter_off:
        original_image = base64.b64decode(b64_clean)
        save_b64_image(full_filename, original_image)
    else:
        print("Not saving original frame, for privacy reasons")

    # Save image with bounding boxes drawn on it
    if something_detected:
        processed_image = analysed_frame.processed_img
        filename_procesed = "{0}_{1}_processed.jpg".format(timestamp, gps_location)
        full_file_name_processed = "{0}/{1}".format(filepath_processed, filename_procesed)
        save_b64_image(full_file_name_processed, processed_image)

    # Return the file location to store this in database
    return full_filename

def save_b64_image(file_name, data):
    with open(file_name, 'wb') as f:
        f.write(data)
        print("File saved called {}".format(file_name))


def privacy_is_okay(counts):
    total_count = counts.get('total')

    if type(total_count) != int:
        return True

    if total_count <= 0:
        return True

    privacy_classes = ["face_privacy_filter", "license_plate_privacy_filter"]
    for class_name in privacy_classes:
        if counts[class_name] > 0:
            return False
    return True

# False: do not save frame
# True: frame may be saved
def privacy_check(counts):
    total_count = counts.get('total')

    if type(total_count) != int:
        return False

    if total_count <= 0:
        return False

    del counts['total']
    privacy_flag = False
    others_flag = False
    
    for k, v in counts.items():
        # if either privacy filter is detected set privacy flag on TRUE
        if (k == 'face_privacy_filter' or k == 'license_plate_privacy_filter') and v > 0:
            privacy_flag = True
        
        # if anything other then privacy filter is detected set other flag TRUE
        if k != 'face_privacy_filter' and k != 'license_plate_privacy_filter' and v > 0:
            others_flag = True

    if privacy_flag is True and others_flag is False:
        return False
    if privacy_flag is False and others_flag is False:
        return False
    else:
        return True

def get_model_version():
    model_folder = "weights"
    first_file_in_weights = os.listdir(model_folder)[0]
    stats = os.stat(model_folder + "/" + first_file_in_weights)
    make_date = datetime.fromtimestamp(stats.st_mtime)
    return make_date.strftime("%Y.%m.%d")